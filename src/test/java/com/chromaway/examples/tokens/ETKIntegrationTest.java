
package com.chromaway.examples.tokens;

import net.postchain.DataLayer;
import net.postchain.core.Transaction;
import net.postchain.gtx.GTXBlockchainConfigurationFactory;
import net.postchain.gtx.GTXDataBuilder;
import net.postchain.gtx.GTXValue;
import net.postchain.test.IntegrationTest;
import nl.komponents.kovenant.Promise;
import org.junit.Assert;
import org.junit.Test;

import javax.xml.bind.DatatypeConverter;
import java.util.ArrayList;
import java.util.List;

import static net.postchain.gtx.ValuesKt.gtx;

public class ETKIntegrationTest extends IntegrationTest {
    byte[] issuerPubKey = bytes("03f811d3e806e6d093a4bcce49c145ba78f9a4b2fbd167753ecab2a13530b081f8");
    byte[] issuerPrivKey = bytes("3132333435363738393031323334353637383930313233343536373839303133");
    byte[] blockchainRID = bytes("5d0502f38c4610e702ee85754f2e5a94c1907c58fc793f3920b2b663ce623fe3");

    private byte[] makeIssueTx(byte[] toAddr, Long amount) {
        byte[][] signers = {issuerPubKey};
        GTXDataBuilder b = new GTXDataBuilder(blockchainRID, signers, getCryptoSystem());
        GTXValue[] gtxValues = {gtx(toAddr), gtx(amount)};
        b.addOperation("etk_issue", gtxValues);
        b.finish();
        b.sign(getCryptoSystem().makeSigner(issuerPubKey, issuerPrivKey));
        return b.serialize();
    }

    private byte[] makeTransferTx(byte[] fromPriv, byte[] fromAddr, byte[] toAddr, Long amount) {
        byte[][] signers = {fromAddr};
        GTXDataBuilder b = new GTXDataBuilder(blockchainRID, signers, getCryptoSystem());
        GTXValue[] gtxValues = {gtx(fromAddr), gtx(toAddr), gtx(amount)};
        b.addOperation("etk_transfer", gtxValues);
        b.finish();
        b.sign(getCryptoSystem().makeSigner(fromAddr, fromPriv));
        return b.serialize();
    }

    @Test
    public void doTest() throws Exception {
        getConfigOverrides().setProperty("blockchain.1.configurationfactory",
                GTXBlockchainConfigurationFactory.class.getName());
        getConfigOverrides().setProperty("blockchain.1.gtx.modules",
                ETKModuleFactory.class.getName());
        getConfigOverrides().setProperty("blockchain.1.gtx.etk.issuerPubKey", hex(issuerPubKey));
        getConfigOverrides().setProperty("blockchain.1.blockchainrid", hex(blockchainRID));

        DataLayer node = createDataLayer(0, 1);

        List<Transaction> validTxs = new ArrayList<Transaction>();
        long[] currentBlockHeight = {-1L};

        byte[] alicePub = pubKey(1);
        byte[] bobPub = pubKey(2);
        byte[] alicePriv = privKey(1);
        byte[] bobPriv = privKey(2);

        // Issue 5000 tokens to Alice
        enqueueTx(node, makeIssueTx(alicePub, 5000L), 0);
        // Can't issue negative amounts
        enqueueTx(node, makeIssueTx(alicePub, -5000L), -1); // -1 means this one is invalid
        buildBlockAndCommit(node);

        enqueueTx(node, makeTransferTx(alicePriv, alicePub, alicePub, 5000L), 1);
        enqueueTx(node, makeTransferTx(alicePriv, alicePub, bobPub, 1000L), 1);
        enqueueTx(node, makeTransferTx(alicePriv, alicePub, bobPub, 10000L), -1);
        enqueueTx(node, makeTransferTx(bobPriv, bobPub, alicePub, 10001L), -1);
        buildBlockAndCommit(node);

        enqueueTx(node, makeTransferTx(bobPriv, bobPub, alicePub, 100L), 2);
        enqueueTx(node, makeTransferTx(alicePriv, bobPub, alicePub, 101L), -1);
        buildBlockAndCommit(node);

        verifyBlockchainTransactions(node);

        Promise<String, Exception> balance = node.getBlockQueries().query("{\"type\"=\"etk_get_balance\"," +
                "\"address\"=\"" + hex(alicePub) + "\"" +
                "}");
        Assert.assertEquals("4100", balance.get());
    }


    private byte[] bytes(String hex) {
        return DatatypeConverter.parseHexBinary(hex);
    }

    private String hex(byte[] bytes) {
        return DatatypeConverter.printHexBinary(bytes).toLowerCase();
    }

}