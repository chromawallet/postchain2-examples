CREATE TABLE etk_balances (
    chain_id BIGINT NOT NULL,
    address BYTEA NOT NULL,
    balance BIGINT NOT NULL,
    UNIQUE (chain_id, address)
);

CREATE TABLE etk_history (
    chain_id BIGINT NOT NULL,
    tx_iid BIGINT NOT NULL REFERENCES transactions(tx_iid),
    op_index INT NOT NULL,
    address BYTEA NOT NULL,
    delta BIGINT NOT NULL
);

CREATE FUNCTION etk_issue
    (chain_id_ BIGINT, tx_iid_ BIGINT, op_index_ INT, toaddr bytea, amount BIGINT)
RETURNS VOID AS $$
BEGIN
    -- update (or insert) recipients balance
    INSERT INTO etk_balances as b (chain_id, address, balance)
    VALUES (chain_id_, toaddr, amount)
    ON CONFLICT (chain_id, address)
    DO UPDATE SET balance = b.balance + amount;

    -- create history entry
    INSERT INTO etk_history (chain_id, tx_iid, op_index, address, delta)
    VALUES (chain_id_, tx_iid_, op_index_, toaddr, amount);
END;
$$ LANGUAGE plpgsql;

CREATE FUNCTION etk_transfer
    (chain_id_ BIGINT, tx_iid_ BIGINT, op_index_ INT, fromaddr BYTEA, toaddr bytea, amount BIGINT)
RETURNS VOID AS $$
DECLARE
    from_balance bigint;
BEGIN
    -- Check if sender has sufficient balance
    SELECT balance INTO from_balance
    FROM etk_balances WHERE chain_id = chain_id_ AND address = fromaddr;
    IF (from_balance IS NULL) OR (from_balance < amount) THEN
        RAISE EXCEPTION 'insufficient balance';
    END IF;

    -- update (or insert) recipients balance
    INSERT INTO etk_balances as b (chain_id, address, balance)
    VALUES (chain_id_, toaddr, amount)
    ON CONFLICT (chain_id, address)
    DO UPDATE SET balance = b.balance + amount;

    -- updates sender's balance
    UPDATE etk_balances SET balance = balance - amount
    WHERE chain_id = chain_id_ AND address = fromaddr;

    -- create history entries
    INSERT INTO etk_history (chain_id, tx_iid, op_index, address, delta)
    VALUES
        (chain_id_, tx_iid_, op_index_, fromaddr, -amount),
        (chain_id_, tx_iid_, op_index_, toaddr, amount);
END;
$$ LANGUAGE plpgsql;