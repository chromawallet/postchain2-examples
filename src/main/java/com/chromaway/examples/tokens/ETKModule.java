package com.chromaway.examples.tokens;

import net.postchain.core.EContext;
import net.postchain.core.Transactor;
import net.postchain.core.UserMistake;
import net.postchain.gtx.*;
import org.apache.commons.dbutils.QueryRunner;
import org.apache.commons.dbutils.handlers.ScalarHandler;
import org.jetbrains.annotations.NotNull;

import java.sql.SQLException;
import java.util.HashSet;
import java.util.Set;

import static net.postchain.gtx.ValuesKt.gtx;

public class ETKModule implements GTXModule {

    private final Set<String> queries = new HashSet<String>();
    private final Set<String> operations = new HashSet<String>();
    private final byte[] issuerPubKey;

    ETKModule(byte[] issuePubKey) {
        operations.add("etk_issue");
        operations.add("etk_transfer");
        queries.add("etk_get_balance");
        this.issuerPubKey = issuePubKey;
    }

    @NotNull
    @Override
    public Set<String> getQueries() { return queries; }

    @NotNull
    @Override
    public Set<String> getOperations() { return operations;  }

    @NotNull
    @Override
    public Transactor makeTransactor(ExtOpData extOpData) {
        String s = extOpData.getOpName();
        if (s.equals("etk_issue")) {
            return new IssueOperation(issuerPubKey, extOpData);
        } else if (s.equals("etk_transfer")) {
            return new TransferOperation(extOpData);
        } else {
            throw new UserMistake("Operation not found", null);
        }
    }

    protected Long getBalance(EContext eContext, byte[] account) {
        QueryRunner r = new QueryRunner();
        ScalarHandler<Long> handler = new ScalarHandler<Long>();
        try {
            return r.query(eContext.getConn(), "SELECT balance FROM etk_balances WHERE chain_id = ? AND address = ?",
                    handler, eContext.getChainID(), account);
        } catch (SQLException e) {
            return 0L;
        }
    }

    @NotNull
    @Override
    public GTXValue query(EContext eContext, String name, GTXValue gtxValue) {
        if (name.equals("etk_get_balance")) {
            return gtx(getBalance(eContext, gtxValue.get("address").asByteArray(true)));
        }
        return gtxValue;
    }

    @Override
    public void initializeDB(EContext eContext) {
        GTXSchemaManager.INSTANCE.autoUpdateSQLSchema(eContext, 0,
                getClass(), "schema.sql", getClass().getName()
        );
    }
}
