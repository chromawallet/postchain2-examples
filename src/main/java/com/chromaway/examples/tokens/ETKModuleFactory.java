package com.chromaway.examples.tokens;

import net.postchain.core.UserMistake;
import net.postchain.gtx.GTXModule;
import net.postchain.gtx.GTXModuleFactory;
import org.apache.commons.configuration2.Configuration;
import org.jetbrains.annotations.NotNull;

import javax.xml.bind.DatatypeConverter;

public class ETKModuleFactory implements GTXModuleFactory {
    @NotNull
    @Override
    public GTXModule makeModule(Configuration configuration) {
        String pkHex = configuration.getString("gtx.etk.issuerPubKey");
        if (pkHex == null) throw new UserMistake("etk.issuerPubKey is not provided", null);
        byte[] issuerPK = DatatypeConverter.parseHexBinary(pkHex);
        return new ETKModule(issuerPK);
    }
}
