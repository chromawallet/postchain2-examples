package com.chromaway.examples.tokens;

import net.postchain.core.TxEContext;
import net.postchain.core.UserMistake;
import net.postchain.gtx.ExtOpData;
import net.postchain.gtx.GTXOperation;
import net.postchain.gtx.GTXValue;
import org.apache.commons.dbutils.QueryRunner;
import org.apache.commons.dbutils.handlers.ScalarHandler;

import java.sql.SQLException;
import java.util.Arrays;

public class IssueOperation extends GTXOperation {

    private final long amount;
    private final byte[] toAddress;
    private final byte[] issuerPubKey;

    IssueOperation(byte[] issuerPubKey, ExtOpData opData) {
        super(opData);
        GTXValue[] args = opData.getArgs();
        toAddress = args[0].asByteArray(false);
        amount = args[1].asInteger();
        this.issuerPubKey = issuerPubKey;
    }

    @Override
    public boolean isCorrect() {
        // check that issuer have signed transaction
        boolean found = false;
        for (byte[] signer : this.getData().getSigners()) {
            if (Arrays.equals(signer, issuerPubKey))
                found = true;
        }
        return found && (amount > 0);
    }

    @Override
    public boolean apply(TxEContext ctx) {
        QueryRunner r = new QueryRunner();
        ScalarHandler<Void> voidHandler = new ScalarHandler<Void>();
        try {
            r.query(ctx.getConn(), "SELECT etk_issue(?, ?, ?, ?, ?)",
                    voidHandler,
                    ctx.getChainID(), ctx.getTxIID(),
                    getData().getOpIndex(),
                    toAddress, amount
            );
        } catch (SQLException e) {
            throw new UserMistake("Error in etk_issue", e);
        }
        return true;
    }
}
