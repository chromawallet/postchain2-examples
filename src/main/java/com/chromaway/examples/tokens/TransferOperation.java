package com.chromaway.examples.tokens;

import net.postchain.core.TxEContext;
import net.postchain.core.UserMistake;
import net.postchain.gtx.ExtOpData;
import net.postchain.gtx.GTXOperation;
import net.postchain.gtx.GTXValue;
import org.apache.commons.dbutils.QueryRunner;
import org.apache.commons.dbutils.handlers.ScalarHandler;

import java.sql.SQLException;
import java.util.Arrays;


public class TransferOperation extends GTXOperation {

    private final long amount;
    private final byte[] fromAddress;
    private final byte[] toAddress;

    TransferOperation(ExtOpData opData) {
        super(opData);
        GTXValue[] args = opData.getArgs();
        fromAddress = args[0].asByteArray(false);
        toAddress = args[1].asByteArray(false);
        amount = args[2].asInteger();
    }

    @Override
    public boolean isCorrect() {
        boolean found = false;
        // check that fromAddress have signed this transaction
        for (byte[] signer : this.getData().getSigners()) {
            if (Arrays.equals(signer, fromAddress))
                found = true;
        }
        // check that amount is positive
        return found && (amount > 0);
    }

    @Override
    public boolean apply(TxEContext ctx) {
        QueryRunner r = new QueryRunner();
        ScalarHandler<Void> voidHandler = new ScalarHandler<Void>();
        try {
            r.query(ctx.getConn(), "SELECT etk_transfer(?, ?, ?, ?, ?, ?)",
                    voidHandler,
                    ctx.getChainID(), ctx.getTxIID(),
                    getData().getOpIndex(),
                    fromAddress, toAddress, amount
            );
        } catch (SQLException e) {
            throw new UserMistake("Error in transfer", e);
        }
        return true;
    }
}
