const postchain = require('postchain-client')
const secp256k1 = require('secp256k1');
const randomBytes = require('crypto').randomBytes;
const util = require('postchain-common').util;

if (process.argv.length != 3) {
	console.log("Usage: " + __filename + " <chainID>");
	process.exit(-1);
}

const pub = Buffer.from("02a0d9fd829af80fd29bd6d76a4d1dcc94c8c2e69709891225cccb45f457381857", "hex");
const priv = Buffer.from("782db762e8a3244aab1693d0bc4c1815da0750ac66df27a33249508f6c660f29", "hex");

const blockchainRID = Buffer.from("78967baa4768cbcef11c508326ffb13a956689fcb6dc3ba17f4b895cbb1577a3", "hex");

const chainID = Buffer.from(process.argv[1], "hex");

const restClient = postchain.restClient.createRestClient("http://localhost:7740", 5);
const gtxClient = postchain.gtxClient.createClient(
    restClient,
    blockchainRID,
    ["esplix_create_chain", "esplix_post_message"]
);

function waitConfirmation(txid) {
        return new Promise( (resolve, reject) => {
            setTimeout(() => {
                console.log("Polling ", txid.toString('hex'));
                restClient.status(txid, (err, res) => {
                    console.log("Got status response:", err, res);
                    if (err) reject(err);
                    else {
                        const {status} = res;
                        switch (status) {
                            case "confirmed": resolve(txid); break;
                            case "rejected": reject(Error("Message was rejected")); break;
                            case "unknown": reject(Error("Server lost our message")); break;
                            case "waiting":
                                waitConfirmation(txid).then(resolve, reject);
                                break;
                            default:
                            console.log(status);
                            reject(Error("got unexpected response from server"));
                    }
                }
            });
        }, 511);
    });
}


function sendAndWaitConfirmation(rq) {
    const buffer = rq.getBufferToSign();
    const txRID = postchain.util.sha256(buffer);
    return (new Promise( (resolve, reject) => {
        rq.send( (error) => {
            if (error) reject(error);
            else resolve(txRID);
        });
    })).then( (txRID) => {
        return waitConfirmation(txRID);
    });
}

function createChain(chainID, payload) {
    const rq = gtxClient.newRequest([pub]);
    rq.esplix_create_chain(chainID, randomBytes(32), payload);
    rq.sign(priv, pub);
    return sendAndWaitConfirmation(rq);
}

function postMessage(prevID, payload) {
    const rq = gtxClient.newRequest([pub]);
    rq.esplix_post_message(prevID, payload);
    rq.sign(priv, pub);
    return sendAndWaitConfirmation(rq);
}

createChain(chainID, randomBytes(100)).then( () => {
    return postMessage(chainID, randomBytes(100));
});
