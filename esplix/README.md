# Postchain2 Esplix script

This script will create a new Esplix chain and one additional message on a Postchain2 database. 

In order to run, one must apply a new and unique chainID for each execution. The blockchainRID, host and port
are set in the script itself.
