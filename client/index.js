const postchain = require('postchain-client')
const secp256k1 = require('secp256k1');
const randomBytes = require('crypto').randomBytes;

function makeKeyPair () {
    let privKey;

    do {
       privKey = randomBytes(32);
    } while (!secp256k1.privateKeyVerify(privKey));

    const pubKey = secp256k1.publicKeyCreate(privKey);
    return {pubKey, privKey};
}

// Create some dummy keys
const aliceKeyPair = makeKeyPair();
const bobKeyPair = makeKeyPair();

const issuerPub = Buffer.from("03f811d3e806e6d093a4bcce49c145ba78f9a4b2fbd167753ecab2a13530b081f8", "hex");
const issuerPriv = Buffer.from("3132333435363738393031323334353637383930313233343536373839303133", "hex");

const blockchainRID = Buffer.from("5d0502f38c4610e702ee85754f2e5a94c1907c58fc793f3920b2b663ce623fe3", "hex");

const restClient = postchain.restClient.createRestClient("http://localhost:7740", 5);
const gtxClient = postchain.gtxClient.createClient(
    restClient,
    blockchainRID,
    ["etk_issue", "etk_transfer"]
);

function waitConfirmation(txid) {
        return new Promise( (resolve, reject) => {
            setTimeout(() => {
                console.log("Polling ", txid.toString('hex'));
                restClient.status(txid, (err, res) => {
                    console.log("Got status response:", err, res);
                    if (err) reject(err);
                    else {
                        const {status} = res;
                        switch (status) {
                            case "confirmed": resolve(txid); break;
                            case "rejected": reject(Error("Message was rejected")); break;
                            case "unknown": reject(Error("Server lost our message")); break;
                            case "waiting":
                                waitConfirmation(txid).then(resolve, reject);
                                break;
                            default:
                            console.log(status);
                            reject(Error("got unexpected response from server"));
                    }
                }
            });
        }, 511);
    });
}


function sendAndWaitConfirmation(rq) {
    const buffer = rq.getBufferToSign();
    const txRID = postchain.util.sha256(buffer);
    return (new Promise( (resolve, reject) => {
        rq.send( (error) => {
            if (error) reject(error);
            else resolve(txRID);
        });
    })).then( (txRID) => {
        return waitConfirmation(txRID);
    });
}

function issueTokens(toAddress, amount) {
    const rq = gtxClient.newRequest([issuerPub]);
    rq.etk_issue(toAddress, amount);
    rq.sign(issuerPriv, issuerPub);
    return sendAndWaitConfirmation(rq);
}

function transferTokens(privKey, fromAddress, toAddress, amount) {
    const rq = gtxClient.newRequest([fromAddress]);
    rq.etk_transfer(fromAddress, toAddress, amount);
    rq.sign(privKey, fromAddress);
    return sendAndWaitConfirmation(rq);
}

issueTokens(aliceKeyPair.pubKey, 10000).then( () => {
    return transferTokens(aliceKeyPair.privKey, aliceKeyPair.pubKey, bobKeyPair.pubKey, 5000);
}).then( () => {
    return transferTokens(bobKeyPair.privKey, bobKeyPair.pubKey, aliceKeyPair.pubKey, 200);
});